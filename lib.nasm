SYS_READ   equ     0          ; read text from stdin
SYS_WRITE  equ     1          ; write text to stdout
SYS_EXIT   equ     60         ; terminate the program
STDIN      equ     0          ; standard input
STDOUT     equ     1		  ; standard output

section .data
numbers: db '0123456789'

section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    mov     rax, SYS_EXIT
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor		rax, rax

	.loop:
		cmp		byte [rdi + rax], 0 ; check if the current symbol is \0
		je		.exit
		inc		rax
		jmp		.loop

	.exit:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:

    push    rdi
    call    string_length
    pop     rsi

	mov		rdx, rax		; string_length function returns length in rax
							; so we put rax value to rdx
    mov     rax, SYS_WRITE
    mov     rdi, STDOUT

    syscall

    ret


; Принимает код символа и выводит его в stdout
print_char:
	push	rdi
	mov		rax, SYS_WRITE
	mov		rsi, rsp
	mov		rdx, 1			; len
	mov		rdi, STDOUT

	syscall

	pop rdi

    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov		rdi, 0xA
	jmp		print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov		rax, rdi
	xor		rcx, rcx				; sign-counter
	mov		r10, 10					; devisor -> r10

	dec		rsp
	mov		byte [rsp], 0			; 0 -> stack for string to be 0-terminated

	.loop:
		xor		rdx, rdx
		div		r10					; rax / r10 -> result in rax, remainder in rdx
		add		rdx, '0'			; convert digit in rdx to its ASCII code
		dec		rsp
		mov		[rsp], dl			; dl -- the smallest part of rdx
		inc		rcx
		test	rax, rax
		jne		.loop

	mov		rdi, rsp				; decimal number pointer -> rdi
	push	rcx						; save rcx before function calling
	call	print_string
	pop		rcx

	add		rsp, rcx				; deallocating memory for deecimal number on stack
	inc		rsp

	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
	test	rdi, rdi
	jns		.positive

	push	rdi
	mov		rdi, '-'
	call	print_char
	pop		rdi
	neg		rdi

	.positive:
		jmp		print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	push	rbx							; length
	push	r12							; str1_pointer
	push	r13							; str2_pointer

	mov		r12, rdi
	mov		r13, rsi

	call	string_length				; str1_pointer is already in rdi
	mov		rbx, rax

	mov		rdi, r13
	call	string_length

	cmp		rax, rbx					; Comparing lengths
	jne		.fail						; return 0 if lengths are not equal

	xor		rax, rax
	.loop:
		cmp		rax, rbx				; rax == length ?
		je		.success

		mov		cl, byte [r12 + rax]	; cl -- the smallest part of rcx, current str1 character

		cmp		cl, byte [r13 + rax]
		jne		.fail

		inc		rax
		jmp		.loop

	.success:
		pop		r13						; callee-saved registers
		pop		r12
		pop		rbx
		mov		rax, 1
		ret

	.fail:
		pop		r13
		pop		r12
		pop		rbx
		xor		rax, rax
		ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	dec		rsp				; Allocating space on stack for 1 byte

	mov		rax, SYS_READ
	mov		rdi, STDIN
	mov		rsi, rsp
	mov		rdx, 1			; the number of bytes to read

	syscall

	test	rax, rax
	jz		.exit

	xor	rax, rax
	mov	al, [rsp]			; al -- the smallest part of rax

	.exit:
		inc		rsp			; Deallocating space on stack
		ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push	rbx					; string length
	push	r12					; buffer pointer
	push	r13					; buffer size

	xor		rbx, rbx
	mov		r12, rdi
	mov		r13, rsi

	.skip_spaces:
		call	read_char

		cmp		al, 0x20		; char == ' ' => skip
		je		.skip_spaces

		cmp		al, 0x9			; char == tab => skip
		je		.skip_spaces

		cmp		al, 0xA			; char == `\n` => skip
		je		.skip_spaces

	.loop:
		test	al, al
		jz		.success

		cmp		rbx, r13
		je		.fail

		cmp		al, 0x20		; char == ' ' => skip
		je		.success

		cmp		al, 0x9			; char == tab => skip
		je		.success

		cmp		al, 0xA			; char == `\n` => skip
		je		.success

		mov		[r12 + rbx], al

		inc		rbx				; string_length++

		call	read_char

		jmp		.loop

	.success:
		cmp		rbx, r13
		je		.fail

		mov		rax, r12
		mov		byte [r12 + rbx], 0
		mov		rdx, rbx

		pop		r13
		pop		r12
		pop		rbx
		ret

	.fail:
		pop		r13
		pop		r12
		pop		rbx

		xor		rax, rax
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push	rbx						; length

	xor		rax, rax				; number
	xor		rbx, rbx
	xor		rcx, rcx
	mov		r10, 10
	.loop:
		mov		cl, [rdi + rbx]

		test	cl, cl
		jz		.exit

		cmp		cl, '0'
		jl		.exit

		cmp		cl, '9'
		jg		.exit

		sub		cl, '0'				; convert digit from ascii code to numeric

		mul		r10					; rax *= 10
		add		rax, rcx			; rax += current digit

		inc		rbx
		jmp		.loop

	.exit:
		mov		rdx, rbx
		pop		rbx
		ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
	mov		r8b, [rdi]				; first symbol (maybe sign) -> r8b

	cmp		r8b, '0'
	jl		.check_sign

	cmp		r8b, '9'
	jg		.fail

	jmp		.parse

	.check_sign:
		cmp		r8b, '+'
		je		.parse

		cmp		r8b, '-'
		je		.parse_negative

	.fail:
		xor		rdx, rdx
		ret

	.parse_negative:
		inc		rdi

	.parse:
		push	r8
		call	parse_uint
		pop		r8

		test	rdx, rdx
		jz		.fail

		cmp		r8b, '-'
		jne		.exit

		neg		rax
		inc		rdx
		ret


	.exit:
		cmp		r8b, '+'
		jne		.exit_no_sign
		inc		rdx

	.exit_no_sign:
		ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	; rdi - string pointer
	; rsi - buffer pointer
	; rdx - buffer size
	xor		rax, rax				; character counter
	.loop:
		cmp		rax, rdx			; counter == buffer size => buffer size is less than string size
		je		.fail

		mov		cl, [rdi + rax]		; current character -> cl

		mov		[rsi + rax], cl		; current character -> buffer
		inc		rax

		cmp		cl, 0
		je		.success

		jmp		.loop

	.fail:
		mov		rax, 0
		ret

	.success:
		ret

