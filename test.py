import unittest
import subprocess


class DictTests(unittest.TestCase):
    
    def test_invalid_input(self):
        key = 'a' * 300
        cmd = f'echo "{key}" | ./program'
        try:
            output = subprocess.check_output(cmd, shell=True, text=True)
            self.assertIn('Key length must be <', output)
        except subprocess.CalledProcessError as e:
            print("Command failed with error: ", e)


    def test_no_key(self):
        cmd = 'echo "kkeeyy" | ./program'
        try:
            output = subprocess.check_output(cmd, shell=True, text=True)
            self.assertIn('KeyNotFoundException', output)
        except subprocess.CalledProcessError as e:
            print("Command failed with error: ", e)


    def test_existing_key(self):
        cmd = 'echo "key3" | ./program'
        try:
            output = subprocess.check_output(cmd, shell=True, text=True)
            self.assertEqual('AAAAAAAAAAAAA', output) 
        except subprocess.CalledProcessError as e:
            print("Command failed with error: ", e)
    

if __name__ == '__main__':
    unittest.main()

