%include "lib.inc"
%include "word.inc"
%include "dict.inc"

%define BUFFER_SIZE 256

section .rodata	; read-only data
no_key_message: db "KeyNotFoundException: no such key", 10, 0
invalid_input_message: db "Key length must be < 255", 10, 0

section .text

global _start

_start:
	push	r12

	sub		rsp, BUFFER_SIZE		; allocating space on stack
	mov		rdi, rsp
	mov		rsi, BUFFER_SIZE
	call	read_word

	test	rax, rax				; check if input was successful
	jz		.invalid_input

	mov		rdi, rsp
	mov		rsi, dict_ptr
	call	find_word

	test	rax, rax
	jz		.key_not_found
	mov		r12, rax				; dict element's pointer -> r12
									; output the value, corresponding to the entered key
	mov		rdi, rsp
	call	string_length
	add		r12, rax				; shifting the pointer from key to value (adding key_length)
	inc		r12						; skipping 0-terminator

	mov		rdi, r12				; value's pointer -> rdi
	jmp		.end


	.invalid_input:
	mov		rdi, invalid_input_message
	jmp		.end

	.key_not_found:
	mov		rdi, no_key_message

	.end:
	call	print_string
	xor		rdi, rdi
	add		rsp, BUFFER_SIZE		; deallocating space on stack
	pop		r12
	jmp		exit
