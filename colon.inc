%define dict_ptr 0			; creating an empty dict pointer  

%macro colon 2

%%self:						; the label of the current element
dq dict_ptr					; setting the pointer of the next element (dq -- 8-byte word)
db %1, 0					; key
%2:							; value's label
%define dict_ptr %%self		; dict_ptr --> last added element 

%endmacro
