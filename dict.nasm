%include "lib.inc"

%define DICT_PTR_SIZE 8

%macro push_regs 1-*
%rep %0
	push %1
	%rotate 1
%endrep
%endmacro

%macro pop_regs 1-*
%rep %0
	%rotate -1
	pop %1
%endrep
%endmacro

section .text

global find_word

; rdi -- key pointer
; rsi -- dict pointer
find_word:
	push_regs	r12, r13
	mov		r12, rdi			; key
	mov		r13, rsi			; dict_ptr

	.loop:	
	mov		r8, r13				; current element's pointer -> r8
	test	r8, r8				; current element's pointer == 0 <=> it's the end of the dict
	jz		.fail
	
	add		r13, DICT_PTR_SIZE  ; shifting the pointer to key 
	mov		rdi, r12			; key pointer -> rdi
	mov		rsi, r13			; current element's key pointer -> rdi
	call	string_equals 
	test	rax, rax
	jnz		.success

	sub		r13, DICT_PTR_SIZE  ; shifting the pointer from key to dict_ptr
	mov		r13, [r13]			; next element's pointer -> r13
	jmp		.loop

	.fail:
	pop_regs	r12, r13
	xor		rax, rax			; return 0 
	ret
	
	.success:
	mov		rax, r13
	pop_regs	r12, r13
	ret


